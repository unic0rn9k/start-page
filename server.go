package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type server struct {
}

func (serv *server) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch strings.ReplaceAll(req.URL.Path, "/", "") {
	default:
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/index.html")
		w.Write(data)
		break
	case "style.css":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/style.css")
		w.Write(data)
		break
	case "g.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/g.png")
		w.Write(data)
		break
	case "ddg.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/ddg.png")
		w.Write(data)
		break
	case "yt.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/yt.png")
		w.Write(data)
		break
	case "s.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/s.png")
		w.Write(data)
		break
	case "wiki.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/wiki.png")
		w.Write(data)
		break
	case "ordnet.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/ordnet.png")
		w.Write(data)
		break
	case "gh.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/gh.png")
		w.Write(data)
		break
	case "gl.png":
		data, _ := ioutil.ReadFile("/home/unic0rn/start-page/gl.png")
		w.Write(data)
		break
	}
}

func main() {
	fmt.Println("opening start page on ':9666'...")
	fmt.Println(http.ListenAndServe(":9666", &server{}))
}
